<?php

require 'CashRefunds.php';

$cr = new CashRefunds\CashRefunds('your api key here', 'your api secret here');

// request example without other parameters
$result = $cr->call('/campaigns');
print_r($result);

// request example with extra parameters
$result = $cr->call('/campaigns', array(
    'offset' => 0,
    'modified_after' => time()
));
print_r($result);