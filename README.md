# README #

This php class can be used to do POST methods to cashrefunds.nl API and retrieve data.

### Requirements ###

* PHP 5.3+
* [cURL](http://curl.haxx.se/)

### Author ###

* Silviu Stan <silviu.stan@gmail.com>