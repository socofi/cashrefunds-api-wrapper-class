<?php

namespace CashRefunds;

/**
 * CashRedunds.nl HTTP API wrapper
 *
 * Uses curl library.
 *
 * @author Silviu Stan <silviu.stan@gmail.com>
 * @version 1.0
 */
class CashRefunds
{
    private $api_key;
    private $api_secret;
    private $api_endpoint = 'https://www.cashrefunds.nl/api';
    private $verify_ssl   = false;

    /**
     * Create a new instance
     * @param string $api_key Your Cashrefunds API key
     * @param string $api_secret Your Cashrefunds API secret
     */
    public function __construct($api_key, $api_secret)
    {
        $this->api_key = $api_key;
        $this->api_secret = $api_secret; //you'll never submit the api_secret as POST value, it will be used only for creation of request_token
    }

    /**
     * Call an API method. Every request needs the API key and Request token, so they are added automatically -- you don't need to pass it in.
     * @param  string $method The API method to call, e.g. '/campaings/1' NO TRAILING SLASH!
     * @param  array  $args   An array of arguments to pass to the method.
     * @return array          Associative array of json decoded API response.
     */
    public function call($method, $args = array(), $result_type = 'array', $timeout = 10)
    {
        return $this->makeRequest($method, $args, $result_type, $timeout);
    }

    /**
     * Performs the HTTP request.
     * @param  string $method The API method to be called
     * @param  array  $args   Assoc array of parameters to be passed
     * @return array          Assoc array of decoded result
     */
    private function makeRequest($method, $args = array(), $result_type, $timeout = 10)
    {
        $args['api_key'] = $this->api_key;
        $args['request_date'] = gmdate('Y-m-d H:i:s');
        $args['request_token'] = $this->createRequestToken($this->api_key, $this->api_secret, $method, $args['request_date']);

        $url = $this->api_endpoint.$method;

        if (function_exists('curl_init') && function_exists('curl_setopt')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-CRAPI/1.0');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->verify_ssl);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($args));
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
            $result = curl_exec($ch);
            curl_close($ch);
        } else {
            die('cURL library is missing.');
        }

        if($result_type == 'array'){
            return $result ? json_decode($result, true) : false;
        }elseif($result_type == 'json'){
            return $result ? $result : false;
        }
    }

    /**
     * Generates the request token.
     * @param  string $api_key The API key
     * @param  string $api_secret The API secret
     * @param  string $method The API method (url)
     * @param  date   $gmt_date GMT date using 'Y-m-d H:i:s' format
     * @return string The request_token hash
     */
    private function createRequestToken($api_key, $api_secret, $method, $gmt_date)
    {

        $data = $api_key.$api_secret.$method.$gmt_date;

        return hash_hmac('sha1', $data, $api_secret);

    }

}